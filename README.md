# README #

This README would normally document whatever steps are necessary to get your application up and running.

### How do I get set up? ###
#### Ubuntu 22
```shell
sudo apt-get update -y && \
sudo apt upgrade -y && \
sudo apt install ansible git -y && \
ansible-pull -i 'localhost,' -U https://bitbucket.org/droptica/scm-ansible-update.git playbook.yml --ask-become-pass --extra-vars "user=$USER"
```
#### Ubuntu 20
```shell
sudo apt install software-properties-common -y && \
sudo apt-add-repository ppa:ansible/ansible -y && \
sudo apt-get update -y && \
sudo apt upgrade -y && \
sudo apt install ansible git -y && \
ansible-pull -i 'localhost,' -U https://bitbucket.org/droptica/scm-ansible-update.git playbook.yml --ask-become-pass --extra-vars "user=$USER"
```

### Who do I talk to? ###

* Krzysztof.Szortyka@droptica.pl
* @devops_team